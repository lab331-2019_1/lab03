import { TestBed } from '@angular/core/testing';

import { NewStudentsFileImplService } from './new-students-file-impl.service';

describe('NewStudentsFileImplService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NewStudentsFileImplService = TestBed.get(NewStudentsFileImplService);
    expect(service).toBeTruthy();
  });
});
